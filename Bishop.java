/** @name Bishop
 ** @author Devinder Singh
 ** this class defines chess Piece Bishop's properties
 */
 
 import java.awt.Graphics2D; 
 
public class Bishop extends Piece
{
	// declaring variables
	private static int id = 0;
	private int type = 3;
	private int color;
	private BackGround b; 
	private int[] availableOption = {-1,-1};

	// constructor will call the super class constructor and will define its name
	public Bishop(int color, int x, int y)
	{
		this.color = color;
		id++;
		if(color == 0)
			b = new BackGround("image/wB.png",x,y);
		else
			b = new BackGround("image/bB.png",x,y);
	}

	public int getID()
	{
		return id;
	}
	
	/** this return the type of the piece
	 ** @return type
	 */
	public int getType()
	{
		return type;
	}

	/** this metod will set x and y of the image*/
	public void setXY(int x, int y)
	{
		b.setXY(x,y);
	}
	
	/**this method will decide can the piece can move or not*/
	public boolean canMove(int dx, int dy,Piece[][] piece)
	{
		int diffx = dx - b.getArrayY();
		int diffy = dy - b.getArrayX();

		if(Math.abs(diffx) == Math.abs(diffy) && piece[dx][dy].getColor() != color)
		{
			int tempx = b.getArrayX();
			int tempy = b.getArrayY();

			while(dx != tempy || dy != tempx)
			{
				tempx = tempx + (diffy/Math.abs(diffy));
				tempy = tempy + (diffx/Math.abs(diffx));
				if(piece[tempy][tempx].getType() != -1 && dx != tempy && dy != tempx)
					return false;
			}
		}
		else	
			return false;
		return true;
	}
	/** this method return the color of the piece
	 ** @return color
	*/
	public int getColor()
	{
		return color;
	}

	public int moveUpTo()
	{
		return 8;
	}

	public int[] pattern(int pattern,int x, int y)
	{
		int [] xy = {x,y};
		
		switch(pattern)
		{
			case 1:
				xy[0]++;
				xy[1]++;
				break;
			case 2:
				xy[0]++;
				xy[1]--;
				break;
			case 3:
				xy[0]--;
				xy[1]++;
				break;
			case 4:
				xy[0]--;
				xy[1]--;
				break;
		}

		return xy;					
	}


	public int numOfPat()
	{
		return 4; 
	}

	/** this method @return p.getX(), the x coords of the piece*/
	public int getX()
	{
		return b.getX();
	}
	
	/** this method @return p.getX(), the y coords of the piece*/
	public int getY()
	{
		return b.getY();
	}

	public void setAvailableOptions(int[] xy)
	{
		availableOption = xy;
	}
	
	public int[] getAvailableOptions()
	{
		return availableOption; 
	}

	/** this method calls the paint method of the backGround class 
	** @param Graphics2D
	*/
	public void paint(Graphics2D g)
	{
		b.paint(g);
	}
}	