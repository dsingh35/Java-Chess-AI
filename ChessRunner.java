/**
 ** @name ChessRunner
 ** @author Devinder Singh
 ** @date 2017-11-07
 */
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JComponent;
import java.awt.Graphics; 
import java.awt.Graphics2D; 
import javax.swing .JOptionPane;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.Timer;
import java.awt.Color;
import java.awt.BorderLayout;
import java.io.*;

/** this class make instance of Myframe and make it visible*/
public class ChessRunner
{ 
	private static final int FRAME_WIDTH = 800;
	private static final int FRAME_HEIGHT = 800;
	
	public static void main(String[] args) throws IOException
	{ 
		MyFrame frame = null;
		try{
			frame = new MyFrame();	
		} 
		catch  (IOException e) {
				JOptionPane.showMessageDialog(null, "There is no Save File");
		}
		catch (NullPointerException e)
		{
			JOptionPane.showMessageDialog(null,"Failed: Please Start Game first");	
		}
		
		catch(ArrayIndexOutOfBoundsException e){}
		
		//frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		//frame.setUndecorated(true);
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	
	}
}
