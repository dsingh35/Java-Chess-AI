/**
 ** @name Board
 ** @author Devinder Singh
 */
import java.util.ArrayList;
import java.awt.Graphics2D; 
import java.io.*;
import java.awt.geom.Rectangle2D;
import java.awt.Color;
import javax.swing.JTextField;

public class Board 
{	
	//defining variables
	private Piece[][] pieces;
	protected static BackGround[][] theBackGround;
	private static ArrayList<BackGround> tempBoard;
	private static BackGround topBorder, bottomBorder,leftBorder,rightBorder;
	private int workAs = 0;
	private ArrayList<Piece> eliminatedPiece;
	private static ArrayList<BackGround> selectedBackGround;
	private static ArrayList<BackGround> possibleBackGround;
	private int eliminatedWhiteX,eliminatedBlackX,eliminatedWhiteY,eliminatedBlackY;
	/** class constructor*/
	public Board() throws IOException,ArrayIndexOutOfBoundsException
	{	
		tempBoard = new ArrayList<BackGround>();
		selectedBackGround = new ArrayList<BackGround>();
		theBackGround = new BackGround[8][8];
		possibleBackGround = new ArrayList<BackGround>();
		pieces = new Piece[8][8];
		eliminatedPiece = new ArrayList<Piece>();
		createBackGround();
	}
	
	/** this method will make the array of background with chess pattern*/
	public void createBackGround()
	{
		for(int i = 1; i <= theBackGround.length; i++)
		{	
			for(int j = 1; j <= theBackGround[i-1].length ; j++)
			{
				if(((i-1) + (j-1)) % 2 == 0)
					theBackGround[i-1][j-1] = new BackGround(BackGround.IMG_BG_BLACK,j*45+400,i*45+150);
				else
					theBackGround[i-1][j-1] = new BackGround(BackGround.IMG_BG_WHITE,j*45+400,i*45+150);
			}
		}
	}	

	/** this method @return pieces, 2d array of Piece*/
	public Piece[][] getPieceslist()
	{
		return pieces;
	}
	
	public void setPieceList(Piece[][] p)
	{
		pieces = p;
	}
	
	/** this returns the piece by given x,y location
	 ** @param x, row the array
	 ** @param y, column of the array
	 ** @return Piece
	 */
	public Piece getPiece(int x, int y)
	{
		return pieces[x][y];
	}
	
	/** this will change the x and y of the given piece
	 ** @param px, row of the array
	 ** @param py, column  of the array
	 ** @param x, the new x coords
	 ** @param y, the new y coords
	 */
	public void animate(int px,int py, int x, int y)
	{
		pieces[px][py].setXY(x,y);
	}
	
	public boolean canMove(int turn,int x, int y) throws ArrayIndexOutOfBoundsException
	{
		if(checkBoundary(x,y) == true)
		{	
			if(pieces[x][y].getColor() == turn || pieces[x][y].getColor() == -1)
				return true;
		}
		return false;
	}	
	
	public void setWorkAs(int workAs)
	{
		this.workAs = workAs;
	}

	/** this will move the piece from given postion to the new given position
	 ** @param from_row, @param from_column , the location from where piece is going to move
     ** @param to_row, @param to_column, the location where the piece should be placed
     */	
	public boolean move(int from_row,int from_column,int to_row,int to_column)
	{
		if(look(from_row,from_column) == true)
		{	
			addEleminatedPieces(to_row,to_column);
			pieces[to_row][to_column] = pieces[from_row][from_column];
			pieces[to_row][to_column].setXY(theBackGround[to_row][to_column].getX(),theBackGround[to_row][to_column].getY());
			remove(from_row,from_column);
			unSelectPiece();
			unSelectPossible();
		}
		return false;	
	}
	
	public void justMove(int from_row,int from_column,int to_row,int to_column)
	{
		System.out.println(to_row + " to_column" + to_column + " from_row " + from_row + " " + from_column);
		addEleminatedPieces(to_row,to_column);
		pieces[to_row][to_column] = pieces[from_row][from_column];
		remove(from_row,from_column);
	}	

	public void undoMove(int from_row,int from_column,int to_row,int to_column)
	{	
		System.out.println("undo" +  to_row + " to_column" + to_column + " from_row " + from_row + " " + from_column);

		pieces[from_row][from_column] = pieces[to_row][to_column];
		pieces[to_row][to_column] = getLastEliminatedPiece();
		pieces[to_row][to_column].setXY(theBackGround[to_row][to_column].getX(),theBackGround[to_row][to_column].getY());	
	}


	public Piece locateKing(int color)
	{
		for(Piece pr[] : pieces)
		{
			for(Piece p : pr)
			{
				if(p.getType() == 1 && p.getColor() != color)
					return p;
			}
		}
		return null;
	}

	public boolean checkSamePiece(int from_row,int from_column,int to_row,int to_column)
	{
		if(from_row == to_row && from_column == to_column)
			return false;

		return true;
	}
	
	public void addEleminatedPieces(int to_row,int to_column)
	{	
		if(pieces[to_row][to_column].getType() == -1)
			return;
		if(pieces[to_row][to_column].getColor() == 0)
		{
			if(eliminatedWhiteX < 195)
			{
				eliminatedWhiteX = 195;
				eliminatedWhiteY = 400;
			}
			else
			{	
				eliminatedWhiteX += 45;
			}
			
			if(eliminatedWhiteX / 555 == 1)
			{
				eliminatedWhiteY -= 45;
				eliminatedWhiteX = 195;
			}

			pieces[to_row][to_column].setXY(eliminatedWhiteY,eliminatedWhiteX);
			
			eliminatedPiece.add(pieces[to_row][to_column]);
		}
		else if(pieces[to_row][to_column].getColor() == 1)
		{	
			if(eliminatedBlackX < 195)
			{
				eliminatedBlackX = 195;
				eliminatedBlackY = 805;
			}
			else
			{	
				eliminatedBlackX += 45;
			}

			if(eliminatedBlackX / 555 == 1)
			{
				eliminatedBlackY += 45;
				eliminatedBlackX = 195;
			}
				
			pieces[to_row][to_column].setXY(eliminatedBlackY,eliminatedBlackX);
			eliminatedPiece.add(pieces[to_row][to_column]);
		}
		else
			eliminatedPiece.add(pieces[to_row][to_column]);	
	}

	public Piece getLastEliminatedPiece()
	{
		Piece p = eliminatedPiece.get(eliminatedPiece.size()-1);
		eliminatedPiece.remove(eliminatedPiece.size()-1);

		return p;
	}

	/** this method will remove the piece from the given location
	 ** @param row, @param column, the location of the piece from where you want to remove the piece
	 */
	public void remove(int row,int column)
	{
		pieces[row][column] = new Dummy();  
	}
	
	/** this method will search for the given location*/
	public boolean look(int row, int column)
	{
		if(pieces[row][column].getType() != -1)
			return true;
		
		return false;
	}
	
	/** this method checks the boundary and @return boolean
	** @param row
	** @param column
	*/
	public boolean checkBoundary(int row , int column)
	{
		if(row > 7 || column > 7 || row < 0 || column < 0)
			return false;
		
		return true;
	}

	/** this piece will change the background color of the selected piece
	 ** @param x
	 ** @param y
	 */
	public static void selectPiece(int x,int y)
	{
		//tempBoard.add(theBackGround[x][y]);
		selectedBackGround.add(new BackGround(BackGround.IMG_BG_SELECTED,theBackGround[x][y].getX(),theBackGround[x][y].getY()));
	}	
	
	public static void selectPossible(int x,int y)
	{
		//tempBoard.add(theBackGround[x][y]);
		possibleBackGround.add(new BackGround(BackGround.IMG_BG_POSSIBLE,theBackGround[x][y].getX(),theBackGround[x][y].getY()));
	}
	
	public static void unSelectPiece()
	{
		selectedBackGround.clear();	
	}
	
	public static void unSelectPossible()
	{
		possibleBackGround.clear();	
	}

	/** this method calls every piece to print itself*/
	public void paint(Graphics2D g2)
	{		
		// Ask each background to print itself
		for(BackGround [] e : theBackGround)
		{
			for(BackGround b : e )
			{
				b.paint(g2);
			}
		}

		for(int i = 0; i < selectedBackGround.size(); i++)
			selectedBackGround.get(i).paint(g2);

		for(int i = 0; i < possibleBackGround.size(); i++)
			possibleBackGround.get(i).paint(g2);
			
		for(Piece p : eliminatedPiece)
		{
			p.paint(g2);
		}		
	}
}