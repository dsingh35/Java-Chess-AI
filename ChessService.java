/**
** @name ChessService
** @author Devinder Singh
** @date 2017-11-30
*/
import java.io.InputStream;
import java.io.*;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;

/** this class provides service to every client by using the thread
*/
public class ChessService implements Runnable
{
	private Socket socket1,socket2;
	private BufferedReader in1,in2;
	private PrintWriter out1,out2;
  private int turn = 0;

	public ChessService(Socket s1, Socket s2)
	{
		socket1 = s1;
		socket2 = s2;
    System.out.println("i am in");
	}

	/** run method of the thread*/
	public void run()
	{
	    try
      	{
        	try
        	{
          	in1 = new BufferedReader(new InputStreamReader(socket1.getInputStream()));// getting the input stream from the socket
          	out1 = new PrintWriter(socket1.getOutputStream());// getting the output stream from the socket
            in2 = new BufferedReader(new InputStreamReader(socket2.getInputStream()));// getting the input stream from the socket
            out2 = new PrintWriter(socket2.getOutputStream());// getting the output stream from the socket 
            out1.println("0");
            out2.println("1");
            out1.flush();
            out2.flush();
            out2.println(in1.readLine());
            out2.flush();
            out1.println(in2.readLine());
            out1.flush();
            
            while(true)
            {  
              if(turn % 2 != 0)
                doService(in1,out2);
              else
                doService(in2,out1);
            }                
         	}

         	finally
         	{
              socket1.close();
          	  socket2.close();
         	}
      	}
      	catch (IOException exception)
      	{
         	exception.printStackTrace();
      	}
        catch(InterruptedException e)
        {

        }
  }

   /**
   	** Executes all commands until the QUIT command or the
    ** end of input.
    */
  public void doService(BufferedReader inx, PrintWriter outx) throws IOException,InterruptedException
  {    
    System.out.println("doService");  
    //int id = in.nextInt();
    while(!inx.ready())
     {

     } 
     System.out.println("getting input"); 
    String response = inx.readLine();
    System.out.println("response " + response);
    outx.println(response);
    outx.flush();  
    turn++;
  }
}  

