/** @name Piece
 ** @author Devinder Singh
 ** this abstarct class is the base for the all chess pieces as it defines some methods those are needed by every method
 */
 
 import java.awt.Graphics2D; 
 
public abstract class Piece
{
	abstract public int getID();
	abstract public void paint(Graphics2D g);
	abstract public int getType(); // that returns the type of the piece such as PAWN
	abstract public int getColor();
	abstract public int getX();
	abstract public int getY();
	abstract public void setXY(int x, int y);
	abstract public boolean canMove(int dx, int dy,Piece[][] piece);
	abstract public int[] pattern(int pattern,int x, int y);
	abstract public int moveUpTo();
	abstract public int numOfPat();
	abstract public void setAvailableOptions(int[] xy);
	abstract public int[] getAvailableOptions();
}	