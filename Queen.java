/** @name Queen
 ** @author Devinder Singh
 ** this class defines chess Piece Queen's properties
 */
 import java.awt.Graphics2D;
 
public class Queen extends Piece
{
	// declaring variables
	private static int id = 0;
	private int type = 2;
	private int color;
	private BackGround p;
	private int[] availableOption = {-1,-1}; 
	
	// constructor will call the super class constructor and will define its name
	public Queen(int color, int x, int y)
	{
		this.color = color;
		id++;
		if(color == 0)
			p = new BackGround("image/wQ.png",x,y);
		else
			p = new BackGround("image/bQ.png",x,y);
	}
	
	/** this return the type of the piece
	 ** @return type
	 */
	public int getType()
	{
		return type;
	}

	public int getID()
	{
		return id;
	}
	
	/** this metod will set x and y of the image*/
	public void setXY(int x, int y)
	{
		p.setXY(x,y);
	}
	
	public int moveUpTo()
	{
		return 7;
	}

	public int[] pattern(int pattern,int x, int y)
	{
		int [] xy = {x,y};

		switch(pattern)
		{
			case 1:
				xy[0]++;
				xy[1]++;
				break;
			case 2:
				xy[0]++;
				xy[1]--;
				break;
			case 3:
				xy[0]--;
				xy[1]++;
				break;
			case 4:
				xy[0]--;
				xy[1]--;
				break;
			case 5:
				xy[0]++;
				break;
			case 6:
				xy[1]++;
				break;	
			case 7:
				xy[0]--;
				break;
			case 8:
				xy[1]--;
				break;			
		}
		return xy;				
	}

	public int numOfPat()
	{
		return 8; 
	}

	/**this method will decide can the piece can move or not*/
	public boolean canMove(int dx, int dy,Piece[][] piece)
	{
		int diffx,diffy = 0;
		int tempx = Math.abs(dx - p.getArrayY());
		int tempy = Math.abs(dy - p.getArrayX());

		if(((tempx < 8 && tempy == 0) || (tempx == 0 && tempy < 8)) && piece[dx][dy].getColor() != color)
		{
			tempy = p.getArrayY();
			tempx = p.getArrayX();

			diffx = dx - tempy;
			diffy = dy - tempx;
			
			if(diffx == 0 && diffy == 0)
				return false;

			while(dx != tempy || dy != tempx)
			{				
				if(diffy != 0)
					tempx = tempx + (diffy/Math.abs(diffy));
				if(diffx != 0)
					tempy = tempy + (diffx/Math.abs(diffx));
				
				if(piece[tempy][tempx].getType() != -1 && (dx != tempy || dy != tempx))
					return false;
			}
		}	
		else
		{	
			diffx = dx - p.getArrayY();
			diffy = dy - p.getArrayX();

			if(diffx == 0 && diffy == 0)
				return false;
			
			if(Math.abs(diffx) == Math.abs(diffy) && piece[dx][dy].getColor() != color)
			{
				tempx = p.getArrayX();
				tempy = p.getArrayY();
				while(dx != tempy || dy != tempx)
				{
					tempx = tempx + (diffy/Math.abs(diffy));
					tempy = tempy + (diffx/Math.abs(diffx));
					if(piece[tempy][tempx].getType() != -1 && (dx != tempy && dy != tempx))
						return false;
				}	
			}
			else
				return false;
		}			
		return true;
	}
	
	/** this method return the color of the piece
	 ** @return color
	*/
	public int getColor()
	{
		return color;
	}
	
	/** this method @return p.getX(), the x coords of the piece*/
	public int getX()
	{
		return p.getX();
	}
	
	/** this method @return p.getX(), the y coords of the piece*/
	public int getY()
	{
		return p.getY();
	}
	
	public void setAvailableOptions(int[] xy)
	{
		availableOption = xy;
	}

	public int[] getAvailableOptions()
	{
		return availableOption;
	}
	
	/** this method calls the paint method of the backGround class 
	** @param Graphics2D
	*/
	public void paint(Graphics2D g)
	{
		p.paint(g);
	}
}	