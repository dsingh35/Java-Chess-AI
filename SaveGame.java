import java.io.*;
import java.util.*;

/** this class will save and load the game file*/
public class SaveGame
{
	/** decloaring variable*/
	private final String saveFile = "SaveFile.txt";
	private PrintWriter write;
	private Scanner inFile;	
	private String line;
	private String save;
	private int valueOfLine;
	private String[] temp;
	private ArrayList<Integer> list;
	
	/** class constructor*/
	public SaveGame() throws IOException
	{
		inFile = new Scanner(new File(saveFile));
		write = new PrintWriter(new File(saveFile));
	}
	
	/** this method will get the model array that has all the pieces and a array 
	 ** we want to change 
	 ** @param original, the the model array
	 ** @param pieces, the array we want to change
	*/
	public void loadGame(Piece[][] pieces) throws IOException
	{
		getLines();
		
		while(list.size() > 3)
		{	
			for(int i = 0; i < 8; i++)
			{
				for(int j = 0; j < 8; j++)
				{
					if(list.get(0) == -1)
					{
						pieces[i][j] = new Dummy();
					}
					else if(list.get(0) == 1)
					{
						pieces[i][j] = new King(list.get(1),list.get(2),list.get(3));
					}
					else if(list.get(0) == 2)
					{
						pieces[i][j] = new Queen(list.get(1),list.get(2),list.get(3));
					}	
					else if(list.get(0) == 3)
					{
						pieces[i][j] = new Bishop(list.get(1),list.get(2),list.get(3));
					}
					else if(list.get(0) == 4)
					{
						pieces[i][j] = new Knight(list.get(1),list.get(2),list.get(3));
					}
					else if(list.get(0) == 5)
					{
						pieces[i][j] = new Rook(list.get(1),list.get(2),list.get(3));
					}
					else if(list.get(0) == 6)
					{
						pieces[i][j] = new Pawn(list.get(1),list.get(2),list.get(3));
					}	
					
					list.remove(0);
					list.remove(0);
					list.remove(0);
					list.remove(0);
				}		
			}
		}	
	}
	
	
	/** this method will read the every line from the game file and then 
	 ** store it to an ArrayList*/
	public void getLines() throws IOException
	{
		inFile = new Scanner(new File(saveFile));
		
		list = new ArrayList<Integer>();
		
		while(inFile.hasNextLine())
		{
			line = inFile.nextLine();	
			line.trim();
			temp = line.split(":");
			valueOfLine  = Integer.parseInt(temp[0]);
			list.add(valueOfLine);
			valueOfLine  = Integer.parseInt(temp[1]);
			list.add(valueOfLine);
			valueOfLine  = Integer.parseInt(temp[2]);
			list.add(valueOfLine);
			valueOfLine  = Integer.parseInt(temp[3]);
			list.add(valueOfLine);
		}
		inFile.close();
	}
	
	/** this method will save the array of game pieces to a file
	 ** pieces, the array array in which we are playing with
	*/
	public void saveGame(Piece[][] pieces) throws IOException
	{
		write = new PrintWriter(new File(saveFile));
		
		for(Piece[] p : pieces)
			for(Piece e : p)
				write.println(e.getType() + ":" + e.getColor() + ":" + e.getX() + ":" + e.getY());
		
		write.close();		
		}
}	