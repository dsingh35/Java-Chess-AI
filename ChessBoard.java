/** @name ChessBoard
 ** @author Devinder Singh
 */
import java.util.ArrayList;
import java.awt.Graphics2D;
import java.io.*;
import javax.swing.JOptionPane;

public class ChessBoard extends Board
{		
	private Piece[][] modelPieces;
	private SaveGame sg;
	
	public ChessBoard() throws IOException,	ArrayIndexOutOfBoundsException
	{	
		super();
		modelPieces = new Piece[8][8];
		placePieces();
		startingPosition();
		sg = new SaveGame();
	}
	
	/** this method will create a model array of pieces by creating each piece instance */		
	public void placePieces()
	{
		modelPieces[0][0] = new Rook(0,theBackGround[0][0].getX(),theBackGround[0][0].getY());
		modelPieces[0][1] = new Knight(0,theBackGround[0][1].getX(),theBackGround[0][1].getY());
		modelPieces[0][2] = new Bishop(0,theBackGround[0][2].getX(),theBackGround[0][2].getY());
		modelPieces[0][3] = new Queen(0,theBackGround[0][3].getX(),theBackGround[0][3].getY());
		modelPieces[0][4] = new King(0,theBackGround[0][4].getX(),theBackGround[0][4].getY());
		modelPieces[0][5] = new Bishop(0,theBackGround[0][5].getX(),theBackGround[0][5].getY());
		modelPieces[0][6] = new Knight(0,theBackGround[0][6].getX(),theBackGround[0][6].getY());
		modelPieces[0][7] = new Rook(0,theBackGround[0][7].getX(),theBackGround[0][7].getY());
		
		for(int i = 0; i < 8; i++)
			modelPieces[1][i] =  new Pawn(0,theBackGround[1][i].getX(),theBackGround[1][i].getY()); ;
		
		for(int i = 2; i < 6; i++)
		{
			for(int j = 0; j < 8; j++)
				modelPieces[i][j] = new Dummy();
		}

		modelPieces[7][0] = new Rook(1,theBackGround[7][0].getX(),theBackGround[7][0].getY());
		modelPieces[7][1] = new Knight(1,theBackGround[7][1].getX(),theBackGround[7][1].getY());
		modelPieces[7][2] = new Bishop(1,theBackGround[7][2].getX(),theBackGround[7][2].getY());
		modelPieces[7][3] = new Queen(1,theBackGround[7][3].getX(),theBackGround[7][3].getY());
		modelPieces[7][4] = new King(1,theBackGround[7][4].getX(),theBackGround[7][4].getY());
		modelPieces[7][5] = new Bishop(1,theBackGround[7][5].getX(),theBackGround[7][5].getY());
		modelPieces[7][6] = new Knight(1,theBackGround[7][6].getX(),theBackGround[7][6].getY());
		modelPieces[7][7] = new Rook(1,theBackGround[7][7].getX(),theBackGround[7][7].getY());
		
		for(int i = 0; i < 8; i++)
			modelPieces[6][i] =  new Pawn(1,theBackGround[6][i].getX(),theBackGround[6][i].getY()); ;
	}
	
	/** this method will set the Chess Pieces to the starting position on chess board
	 */
	public void startingPosition()
	{
		for(int i =0; i < 8; i++)
		{	
			for(int j = 0; j < 8; j++)
			{
				getPieceslist()[i][j] = modelPieces[i][j];
			}
		}
	}		
	
	/** this method will ask the user to confirm fro loading and theload the game by calling 
	** load method of saveGame
	*/
	public void loadGame() throws IOException, NullPointerException
	{
		int response = JOptionPane.showConfirmDialog(null, "All unsaved data will be deleted, Do you want to proceed?", "Warning",
		JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			
		if (response == JOptionPane.YES_OPTION) 
		{	
			sg.loadGame(getPieceslist());
		}	
	}
	
	/** this method will save the game by calling the save game method in Save Game class*/
	public void saveGame() throws IOException , NullPointerException
	{
		sg.saveGame(getPieceslist());
		JOptionPane.showMessageDialog(null, "Game has been saved");
	}
	
	/** this method checks if piece has the ability to move that far or not*/
	public boolean canMove(int turn,int x, int y, int dx, int dy) throws ArrayIndexOutOfBoundsException
	{
		if(getPieceslist()[x][y].getColor() == turn && getPieceslist()[dx][dy].getColor() != turn)
			return getPieceslist()[x][y].canMove(dx,dy,getPieceslist());
		return false;
	}
	
	/** tell every piece to draw itself*/
	public void paint(Graphics2D g)
	{
		super.paint(g);
		
		for(Piece[] e : getPieceslist())
			for(Piece p : e)
				p.paint(g);
	}
}
