/** @name MyFrame
 ** @author Devinder Singh
 ** @date 2017-11-07
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.*;
import java.util.*;
import java.io.*;

/** this class extends JFrame and add listeners to the methods of Board*/
public class MyFrame extends JFrame
{
	final JPanel mainPanel = new JPanel();
	final Buttons button = new Buttons();
	final ChessComponent tempcomponent;
	private ChessComponent component;
	
	/** class constructor*/
	public MyFrame() throws IOException, ArrayIndexOutOfBoundsException
	{
		tempcomponent = new ChessComponent("temp");
		setLayout(new BorderLayout());
		add(button, BorderLayout.WEST);
		add(tempcomponent,BorderLayout.CENTER);
		addListeners();
	}
	
	class StartListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try{
				remove(tempcomponent);// removes temporary component
				component = new ChessComponent(); // makes a new chesscomponent
				add(component,BorderLayout.CENTER); // add it to the frame
			}
			catch(IOException e)
			{
			}		
		}
	}
	
	class ReStartListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try{
				remove(component); // remove the actual component
				component = new ChessComponent();  
				add(component,BorderLayout.CENTER);
			}
			catch(IOException e) {
			}
			
		}
	}
	
	class LoadListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			try{
					component.loadGame();
					component.repaint();
			
			} catch  (IOException e) {
				JOptionPane.showMessageDialog(null, "There is no Save File");
		}
		
		catch (NullPointerException e)
		{
			JOptionPane.showMessageDialog(null,"Failed: Please Start Game first");	
		}
		
		}
	}
	
	class SaveListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
				try{
					component.saveGame();
			
			} catch  (IOException e) {
			
			JOptionPane.showMessageDialog(null,"Failed: some error occured");		
			}
			
			catch (NullPointerException e)
			{
				JOptionPane.showMessageDialog(null,"Failed: Please Start Game first");	
			}
		}
	}
	
	class QuitListener implements ActionListener
	{
		/** this will quit the application */
		public void actionPerformed(ActionEvent event)
		{
			int response = JOptionPane.showConfirmDialog(null, "All unsaved data will be deleted, Do you want to proceed?", "Warning",
			JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			
			if (response == JOptionPane.YES_OPTION) 
				System.exit(0);
		}
	}
	
	/** this for the online button*/
	class OnlineListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			component.goOnline();
		}
	}
	/** this will add the listeners to the buttons*/
	public void addListeners()
	{
		ActionListener startListener = new StartListener();
		button.getStartGameButton().addActionListener(startListener);
		
		ActionListener ReStartListener = new ReStartListener();
		button.getReStartGameButton().addActionListener(ReStartListener);
		
		ActionListener loadListener = new LoadListener();
		button.getLoadGameButton().addActionListener(loadListener);
		
		ActionListener saveListener = new SaveListener();
		button.getSaveGameButton().addActionListener(saveListener);
		
		ActionListener quitListener = new QuitListener();
		button.getQuitGameButton().addActionListener(quitListener);
		
		ActionListener onlineListener = new OnlineListener();
		button.getOnlineButton().addActionListener(onlineListener);
		

	}	
}	