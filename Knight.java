/** @name Knight
 ** @author Devinder Singh
 ** this class defines chess Piece Knight's properties
 */
import java.awt.Graphics2D; 

public class Knight extends Piece
{
	// declaring variables
	private static int id = 0;
	private int type = 4;
	private int color;
	private BackGround p; 
	private int[] availableOption = {-1,-1};
	
	// constructor will call the super class constructor and will define its name
	public Knight(int color, int x, int y)
	{
		this.color = color;
		id++;
		if(color == 0)
			p = new BackGround("image/wKn.png",x,y);
		else
			p = new BackGround("image/bKn.png",x,y);
	}
	
	public int getID()
	{
		return id;
	}
	
	/** this return the type of the piece
	 ** @return type
	 */
	public int getType()
	{
		return type;
	}


	/** this metod will set x and y of the image*/
	public void setXY(int x, int y)
	{
		p.setXY(x,y);
	}
	
		/**this method will decide can the piece can move or not*/
	public boolean canMove(int dx, int dy,Piece[][] piece)
	{
		if((Math.abs(dy - p.getArrayX()) == 2 && Math.abs(dx - p.getArrayY()) == 1) || (Math.abs(dy - p.getArrayX()) == 1 && Math.abs(dx - p.getArrayY()) == 2))
			if(piece[dx][dy].getColor() != color)
				return true;
		return false;
	}

	/** this method return the color of the piece
	 ** @return color
	*/
	public int getColor()
	{
		return color;
	}
	
	/** this method @return p.getX(), the x coords of the piece*/
	public int getX()
	{
		return p.getX();
	}
	
	public int moveUpTo()
	{
		return 1;
	}

	public int[] pattern(int pattern,int x, int y)
	{
		int [] xy = {x,y};
		
		switch(pattern)
		{
			case 1:
				xy[0]+=2;
				xy[1]++;
				break;
			case 2:
				xy[0]-=2;
				xy[1]--;
				break;
			case 3:
				xy[0]+=2;
				xy[1]--;
				break;
			case 4:
				xy[0]-=2;
				xy[1]++;
				break;
			case 5:
				xy[0]++;
				xy[1]-=2;
				break;
			case 6:
				xy[0]++;
				xy[1]+=2;
				break;
			case 7:
				xy[0]--;
				xy[1]-=2;
				break;
			case 8:
				xy[0]--;
				xy[1]+=2;
				break;			
		}	
		return xy;				
	}

	public int numOfPat()
	{
		return 8; 
	}

	public void setAvailableOptions(int[] xy)
	{
		availableOption = xy;
	}

	public int[] getAvailableOptions()
	{
		return availableOption;
	}
	
	/** this method @return p.getX(), the y coords of the piece*/
	public int getY()
	{
		return p.getY();
	}
	
	/** this method calls the paint method of the backGround class 
	** @param Graphics2D
	*/
	public void paint(Graphics2D g)
	{
		p.paint(g);
	}
}	