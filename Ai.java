import java.util.*;

public class Ai
{
	private int color;
	private ChessBoard board;
	private int[] availableOption = {-1,-1};
	private Piece bestMove = null;
	private boolean checkMate = false;
	private ArrayList<Piece> kingKiller;
	private Clock[] clocks;
	private	ArrayList<Integer> escapeKingList = new ArrayList<Integer>();
	private ArrayList<Piece> extTarget = new ArrayList<Piece>();
	private	ArrayList<Piece> bestMoves = new ArrayList<Piece>();

	public Ai(int color, ChessBoard board, Clock[] clock)
	{
		this.color = color;
		this.board = board;
		kingKiller = new ArrayList<Piece>();
		clocks = clock;
	}

	public void setAvailableMoves()
	{
		for(Piece pi[] : board.getPieceslist())
			for(Piece p : pi)
				p.setAvailableOptions(onClicked(p,false,false));	
	}

	public Piece chooseBestMove(int color, boolean ret)
	{
		Piece bestTarget = null;
		Piece tempTarget = null;
		bestMove = null;

		for(Piece pi[] : board.getPieceslist())
		{	for(Piece p : pi)
			{
				if(p.getColor() == color && p.getAvailableOptions()[0] != -1)
				{	
					tempTarget = board.getPiece(p.getAvailableOptions()[0],p.getAvailableOptions()[1]);
					
					if(bestMove == null)
					{
						bestMove = p;
						bestTarget = board.getPiece(bestMove.getAvailableOptions()[0],bestMove.getAvailableOptions()[1]);
					}						
					if(bestTarget.getType() == -1)
						bestMove = p;
					else if(tempTarget.getType() != -1 && noExpTarget(tempTarget) == true && tempTarget.getType() < bestTarget.getType())
						bestMove = p;
					
					bestTarget = board.getPiece(bestMove.getAvailableOptions()[0],bestMove.getAvailableOptions()[1]);
					bestMoves.add(bestMove);
					
					if(bestTarget.getType() == 1)
					{	
						checkMate(p);
					}
				}		 
			}	
		}

		if(color == 1 && pieceCount(0) < 4)
			return killKing(board.locateKing(0));

		if(bestTarget.getType() == -1)
		{
			bestMove = bestMoves.get((int)(0+Math.random()*(bestMoves.size()-1)));
		}
		else if(ret == false)
		{
			board.justMove((int)((bestMove.getY()- ChessComponent.X)/45)-1,(int)((bestMove.getX()-ChessComponent.Y)/45)-1,(int)((bestTarget.getY()-ChessComponent.X)/45)-1,(int)((bestTarget.getX()- ChessComponent.Y)/45)-1);
			setAvailableMoves();
			Piece counterAttack = chooseBestMove(0,true);
			if(board.getPiece(counterAttack.getAvailableOptions()[0],counterAttack.getAvailableOptions()[1]).getType() < bestTarget.getType())
			{
				System.out.println("bacsdhvsjhvcsdj");
				board.undoMove((int)((bestMove.getY()- ChessComponent.X)/45)-1,(int)((bestMove.getX()-ChessComponent.Y)/45)-1,(int)((bestTarget.getY()-ChessComponent.X)/45)-1,(int)((bestTarget.getX()- ChessComponent.Y)/45)-1);
				extTarget.add(board.getPiece((int)((bestTarget.getY()-ChessComponent.X)/45)-1,(int)((bestTarget.getX()- ChessComponent.Y)/45)-1));
				setAvailableMoves();
				return chooseBestMove(1,true);
			}	
		}	

		extTarget.clear();
		return bestMove;
	}

	public int pieceCount(int color)
	{
		int counter = 0;

		for (Piece e[] : board.getPieceslist()) {
			for (Piece p : e) {
				if(p.getColor() == color)
					counter++;
			}
			
		}

		return counter;
	}

	public Piece killKing(Piece king)
	{
		for(Piece p : bestMoves)
		{
			
		}	
	}

	public boolean noExpTarget(Piece target)
	{
		for(Piece p : extTarget)
		{
			if(p.getType() == target.getType() && p.getID() == target.getID())
				return false;
		}	

		return true;
	}

	public void checkMate(Piece p)
	{
		System.out.println("checkMate");
		clocks[(int)(p.getColor() % 2)].setCheckMate();
		if (p.getColor() == 0) {
			kingKiller.clear();
		kingKiller.add(p);		
		}
	}

	public boolean isKingInDanger(){
		if(kingKiller.size() > 0)
			return true;
		return false;
	}

	public void resetCheckMate()
	{
		checkMate = false;
	}

	public Piece escapeKing()
	{	
		escapeKingList.clear();

		if(kingKiller.size() > 0)
		{
			
			for(Piece pi[] : board.getPieceslist())
			{
				for(Piece p : pi)
				{	
					if(p.getColor() == color && p.getAvailableOptions()[0] != -1 && board.getPiece(p.getAvailableOptions()[0],p.getAvailableOptions()[1]).getID() == kingKiller.get(0).getID())
					{
						int[] moveCoords = {(int)(p.getY()-ChessComponent.X)/45-1,(int)(p.getX()-ChessComponent.Y)/45-1,p.getAvailableOptions()[0],p.getAvailableOptions()[1]};

						board.justMove(moveCoords[0],moveCoords[1],moveCoords[2],moveCoords[3]);
						setAvailableMoves();
						chooseBestMove(0,true);
						board.undoMove(moveCoords[0],moveCoords[1],moveCoords[2],moveCoords[3]);		
					
						if(kingKiller.size() <= 0)
						{
							System.out.println("line 103 escapeKing");
							int[] tmp = {moveCoords[2],moveCoords[3]};
							p.setAvailableOptions(tmp);
							return p;
						}
					}
				}
			}	
						
			Piece king = board.locateKing(0);
			return moveKing(king);
		}

		return null;			
	} 

	public Piece moveKing(Piece p)
	{
		onClicked(p,false,true);

			
			System.out.println("line 131 moveKing " + escapeKingList.size());

			for(int i = 0; i < escapeKingList.size(); i+=2)
			{
				int[] kingOriginalLoc = {(int)((p.getY()-ChessComponent.X)/45-1),(int)((p.getX()-ChessComponent.Y)/45-1)};
				System.out.println("escapeKing " + escapeKingList.get(i) + " " + escapeKingList.get(i+1));
				board.justMove(kingOriginalLoc[0],kingOriginalLoc[1],escapeKingList.get(i),escapeKingList.get(i+1));
				kingKiller.clear();
				setAvailableMoves();
				chooseBestMove(0,true);
				
				if(kingKiller.size() <= 0)
				{
					System.out.println("King moved ");
					board.undoMove(kingOriginalLoc[0],kingOriginalLoc[1],escapeKingList.get(i),escapeKingList.get(i+1));
					int[] tmp = {escapeKingList.get(i),escapeKingList.get(i+1)};
					p.setAvailableOptions(tmp);
					return p;
				}
	
				board.undoMove(kingOriginalLoc[0],kingOriginalLoc[1],escapeKingList.get(i),escapeKingList.get(i+1));	
			}

		System.out.println("line 131 moveKing");

	  	return tryShieldingKing(p);
	}

	public Piece tryShieldingKing(Piece king)
	{
		int[] kingXY = {(king.getY() - ChessComponent.X)/45-1,(king.getX() - ChessComponent.Y)/45-1};
		int[] disBtwXY = {(kingKiller.get(0).getY() - ChessComponent.X)/45-1,(kingKiller.get(0).getX() - ChessComponent.Y)/45-1};   
		int signX = kingXY[0] - disBtwXY[0];
		int signY = kingXY[1] - disBtwXY[1];

		System.out.println(" kingKiller.size: " + kingKiller.size() + " kingXY: " +  kingXY[0] + "kingXY[1]: " + kingXY[1]);
		if(signX != 0)
		{
			signX = signX/Math.abs(signX);
		}

		if(signY != 0)
		{
			signY = signY/Math.abs(signY);
		}

		System.out.println("signY: " + signY + "signX: " + signX);
		while(disBtwXY[0] != kingXY[0] || disBtwXY[1] != kingXY[1])
		{	
			System.out.println("disBtwXY[0]: disBtwXY[1] :  " + disBtwXY[0] + ": " + disBtwXY[1]);
			if (disBtwXY[0] != kingXY[0] ) {
				disBtwXY[0] = disBtwXY[0] + (signX);
			}

			if (disBtwXY[1] != kingXY[1] ) {
				disBtwXY[1] = disBtwXY[1] + (signY);
			}

			for (Piece pi[] : board.getPieceslist()) {
				for (Piece p : pi) {
					if(p.getColor() == color && p.getType() != 1)
					{
						if(p.canMove(disBtwXY[0],disBtwXY[1],board.getPieceslist()) == true)
						{
							p.setAvailableOptions(disBtwXY);
							System.out.println(" ma ha ta haiga " + p.getType());
							return p;
						}
					}		
				}	
			}
		}
		return null;
	}

	public int[] onClicked(Piece p,boolean showSteps, boolean escapeKing)
	{	
		int availableOption[] = {-1,0};
		int initialY = (int)((p.getY()-150)/45)-1;
		int initialX = (int)((p.getX()-400)/45)-1;

		for(int i = 1; i <= p.numOfPat(); i++)
		{
			boolean go = true;
			int[] possibleMoves = {initialY,initialX};
			int counter = 1;

			while(go == true &&  counter <= p.moveUpTo())
			{
				possibleMoves = p.pattern(i,possibleMoves[0],possibleMoves[1]);
				
				if(board.checkBoundary(possibleMoves[0],possibleMoves[1]) == false)
					go = false;
				else
				{
					if(p.canMove(possibleMoves[0],possibleMoves[1], board.getPieceslist()) == true)
					{
						if(escapeKing == true)
						{
							escapeKingList.add(possibleMoves[0]);escapeKingList.add(possibleMoves[1]);
						}

						if(showSteps == true)
							board.selectPossible(possibleMoves[0],possibleMoves[1]);

							if(board.getPiece(possibleMoves[0],possibleMoves[1]).getType() != -1)
							{	
								if(availableOption[0] == -1 || board.getPiece(availableOption[0],availableOption[1]).getType() == -1)
									availableOption = possibleMoves;
								else if(board.getPiece(possibleMoves[0],possibleMoves[1]).getType() != -1 && board.getPiece(availableOption[0],availableOption[1]).getType() > board.getPiece(possibleMoves[0],possibleMoves[1]).getType())
								{
									availableOption = possibleMoves;								
								}
							}		
							else if(availableOption[0] == -1)
								availableOption = possibleMoves;
					}
					else
						go = false;	
				}	

				counter++;
			}	
		}

		return availableOption;
	}

}	
