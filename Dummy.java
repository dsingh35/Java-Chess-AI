import java.awt.Graphics2D; 

/** @name Dummy
 ** @author Devinder Singh
 ** @date 2017-1107
 */
 
 /** this is class represents the dummy piece that doesn't do anything*/
public class Dummy extends Piece
{
	public int getType()
	{
		return -1;
	}
	
	public int getColor()
	{
		return -1;
	}	
	
	public int getID()
	{
		return -1;
	}
	
	public void paint(Graphics2D g2){
	// Do nothing
	}
	
	public int getX()
	{
		return -1;
	}
	
		/**this method will decide can the piece can move or not*/
	public boolean canMove(int dx, int dy,Piece[][] piece)
	{
		return false;
	}
	
	public int getY()
	{
		return -1;
	}
	
	public void setXY(int x, int y)
	{
	}	

	public int[] pattern(int pattern,int x, int y)
	{
		int [] xy = {x,y};

		switch(pattern)
		{
			case 1:
				xy[0]++;
				xy[1]++;
				break;
			case 2:
				xy[0]++;
				xy[1]--;
				break;
			case 3:
				xy[0]--;
				xy[1]++;
				break;
			case 4:
				xy[0]--;
				xy[1]--;
				break;
			case 6:
				xy[0]++;
				break;		
		}
		return xy;					
	}
	
	public void setAvailableOptions(int[] xy)
	{
	}
	
	public int[] getAvailableOptions()
	{
		return null;
	}
	
	public int moveUpTo()
	{
		return 0;
	}
	public int numOfPat()
	{
		return 0;
	}
}	