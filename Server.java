/**
** @name Server
** @author Devinder Singh
** @date 2017-11-30
*/
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server
{
	public static void main(String[] args) throws IOException
	{
		try
		{
			int port_number = 9997;// if argument is not passed this will be the portNumber
			if(args.length > 0  )// if arguments are passed
				port_number = Integer.parseInt(args[0]);
		
				ServerSocket server = new ServerSocket(port_number);
				
				System.out.println("Waiting for clients to connect");
				
			while (true)// will be live forever until it is terminated
			{ 
				Socket s1 = server.accept();// accepting the input from client
	            System.out.println("Client1 connected.");
				Socket s2 = server.accept();// accepting the input from client
	            System.out.println("Client2 connected.");

	            ChessService service = new ChessService(s1,s2);// intialising the service
	            Thread t = new Thread(service);// creating a new thread for each client
	            t.start();
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}		
	}
}