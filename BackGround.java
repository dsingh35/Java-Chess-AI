import java.awt.Graphics2D; 
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;


public class BackGround
{
	final public static String IMG_BG_BLACK = "image/black.PNG";
	final public static String IMG_BG_WHITE = "image/white.PNG";
	final public static String IMG_BG_SELECTED = "image/selected.PNG";
	final public static String IMG_BG_POSSIBLE = "image/possible.PNG";
		
	private BufferedImage img;
	private int x,y;
	private char color;
	
	public BackGround( String imageFileName, int x, int y)  {
		this.x = x;
		this.y = y;
		this.color = imageFileName.charAt(0);
		
	 	try{
			img = ImageIO.read(new File(imageFileName));
			
		} catch  (IOException e) {
    
		}
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}
	
	public int getArrayX()
	{
		return ((int)(x-400)/45)-1;
	}

	public int getArrayY()
	{
		return ((int)(y-150)/45)-1;
	}
	
	public void setXY(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	
	public char getColor()
	{
		return color;
	}	
	
	public void paint(Graphics2D g) {
		g.drawImage(img,x,y ,img.getWidth(), img.getHeight(), null);  
	}
}	