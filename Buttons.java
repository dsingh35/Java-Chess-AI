import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import java.awt.*;
import javax.swing.JButton;
import java.awt.BorderLayout;

public class Buttons extends JPanel
{
	private JButton reStartGame;
	private JButton startGame;
	private JButton saveGame;
	private JButton loadGame;
	private JButton quitGame;
	private JButton online;
	
	public Buttons()
	{
		createButtons();
		addToPanel();
	}
	
	public void createButtons()
	{
		startGame = new JButton("Start Game");
		reStartGame = new JButton("ReStart Game");
		saveGame = new JButton("Save Game");
		loadGame = new JButton("Load Game");
		quitGame = new JButton("Quit Game");
		online = new JButton("Go online");
		setLayout(new GridLayout(6,1));
	}	
	
	public void addToPanel()
	{
		add(startGame);
		add(reStartGame);
		add(saveGame);
		add(loadGame);
		add(online);
		add(quitGame);
	}
	
	public JButton getStartGameButton()
	{
		return startGame;
	}
	
	public JButton getReStartGameButton()
	{
		return reStartGame;
	}
	
	public JButton getLoadGameButton()
	{
		return loadGame;
	}
	
	public JButton getSaveGameButton()
	{
		return saveGame;
	}
	
	public JButton getQuitGameButton()
	{
		return quitGame;
	}
	
	public JButton getOnlineButton()
	{
		return online;
	}
}	