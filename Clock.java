/** @name Clock
 ** @author Devinder Singh
 ** @date 2017-11-07
 */
 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.*;
import java.awt.BorderLayout;
import java.util.*;

/** this class extends the JPanel and contructs a clock and displays it on the 
 ** frame
 */
public class Clock extends JPanel
{
	private Timer t;
	private JLabel timeL;
	private JLabel label;
	private  String playerName = "Anonymous";
	private boolean inTime = true;
	
	/** class constructor */
	public Clock(String playername)
	{	
		this.playerName = playername;
		label = new JLabel("30");
		setPlayerName();
		label.setFont(new Font("Serif", Font.PLAIN , 24));
		add(label);
		timeL = new JLabel("30");
		timeL.setFont(new Font("Serif", Font.BOLD , 26));
		timeL.setForeground(Color.RED);
		add(timeL);
	}
	
	/** this method will start the timer */
	public void startTimer()
	{
		// Inner class 
      class CountDown implements ActionListener{
		 private int count;
         
		  public CountDown(int initialCount) {
            count = initialCount;
         }
         public void actionPerformed(ActionEvent event) {
            if(count>0)
            {
          		ChessComponent.sound(ChessComponent.TICK);
				timeL.setText("" + count);
				count--;
			}
            else if(count < 1)
			{
				inTime = false;
				timeL.setText("time's up! Player: " + playerName);
				t.stop();
			}
		}
      
      // end of inner class
	 }
		CountDown listener = new CountDown(30);//making the instance of the timer
		final int DELAY = 1000; // Milliseconds between timer ticks
		t = new Timer(DELAY, listener);
		t.start();
	}	
	
	/** this method will return whether timer is stoped before completion or not
	 ** @return inTime
	 */
	public boolean getInTime()
	{
		return inTime;
	}
	
	public void setPlayerName()
	{
		label.setText(playerName + " 's Clock ");
	}

	public void setCheckMate()
	{
		label.setText("Check Mate");
	}
	
	public void setText(String s)
	{
		timeL.setText(s);
	}

	/** this method will simply stop the timer by calling its stop metho*/
	public void stopTimer()
	{
		t.stop();
	}	
}

