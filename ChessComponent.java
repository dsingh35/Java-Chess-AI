/**
 ** @name ChessComponent
 ** @author Devinder Singh
 ** @date 2017-11-07
 */
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import java.awt.Graphics; 
import java.awt.Graphics2D; 
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import javax.swing.Timer;
import java.awt.*;
import java.util.*;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing .JOptionPane;
import java.io.*;
import javax.sound.sampled.*;
import java.io.InputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/** This class will do the actual work for clicking and dragging , it will call methods from Board class to get boundaries and everything
 */
public class ChessComponent extends JComponent
{
	private ChessBoard b;
	private Clock[] clocks = new Clock[2];
	private final int[] MCOORDS = {-1,-1,-1,-1}; // coords where mouse will be clicked
	private	MouseListener listener;
	private MouseMotionListener motionListener;
	private Timer t; // timer for animation
	private String player1Name = "Anonymous 1";
	private String player2Name = "Anonymous 2";
	private boolean drag,select = false;
	private boolean canMove = true; 
	private int draggedX,draggedY,initialX,initialY;
	private final int[] turn = {0}; // piece will be moved acc. it
	public static int X = 150; // the final x where board will be created
	public static int Y = 400;
	private int moves = 0;
	private String serverIP = "192.168.0.89";
	private	int port_number = 5555;
	private Socket s;
	private Scanner in;
	private PrintWriter out;
	private boolean onlineMove,online = false;
	private BufferedReader reader;
	private int workAs;
	private Thread check;
	private final String CLICK = "sounds/click.wav";
	private final String MOVE = "sounds/PieceMove.wav"; 
	public static final String INCORRECT = "sounds/incorrect.wav"; 
	public static final String TICK = "sounds/Tick.wav"; 
	public static final String TICKFAST = "sounds/Tickfast.wav"; 
	private final String START = "sounds/Start.wav"; 
	private ArrayList<Integer> unselect = new ArrayList<Integer>();
	private Ai ai;
	private Thread delayThread;
 
	/** dummy class constructor,it will only draw the board and pieces*/
	public ChessComponent(String temp) throws IOException
	{
		b = new ChessBoard();
	}	
	
	/** class constructor, it will add the listeners and clocks to the component*/
	public ChessComponent() throws IOException
	{
		b = new ChessBoard();
		ai = new Ai(1,b,clocks);
		getPlayer1Name();// getting names from the user
		getPlayer2Name();
		clocks[0] = new Clock(player1Name);
		clocks[1] = new Clock(player2Name);
		setLayout(new BorderLayout());
		add(clocks[0], BorderLayout.NORTH);
		add(clocks[1], BorderLayout.SOUTH);
		listener = new MouseClickListener();
		motionListener = new MouseClickListener();
		addMouseListener(listener);
		addMouseMotionListener(motionListener);
		startGame();// starting the timers
		sound(START);
	}
	
	/** this method will start the clocks acc. to the turn*/
	public void startGame()
	{	
		if(getTurn() == workAs)
		{	
			if(online == false)
			{	
				ai.setAvailableMoves();
				changeMcoords(ai.chooseBestMove(1,true));
				onlineMove = true;
				resetMcoords();
			}	
			else
			{	
				startCheckingInput();
			}

			clocks[getTurn()].startTimer();
		}	
		else 
		{
			clocks[getTurn()].startTimer();	
			onlineMove = true;
			ai.setAvailableMoves();
			ai.chooseBestMove(0,true);
				
				Piece escapeKingPiece = ai.escapeKing();
				if(escapeKingPiece != null)
				{	
					changeMcoords(escapeKingPiece);
					System.out.println("YESADS");
				}	
				else
				{
					changeMcoords(ai.chooseBestMove(1,true));
					while(ai.isKingInDanger() == true)
					{	
						b.justMove(MCOORDS[1],MCOORDS[0],MCOORDS[3],MCOORDS[2]);
						ai.setAvailableMoves();
						ai.chooseBestMove(0,true);
						b.undoMove(MCOORDS[1],MCOORDS[0],MCOORDS[3],MCOORDS[2]);
						changeMcoords(ai.chooseBestMove(1,true));
					}
				}
		
			moveAfterDelay();
		}	
	}
	
	/** this method will remove the listeners*/
	public void timeUp()
	{
		removeMouseListener(listener);
		removeMouseMotionListener(motionListener);
	}
	
	/** this will restart the game by creating new instance field of board and clocks*/
	public void reStart()  
	{
		startGame();
		clocks[0].stopTimer();
		clocks[1].stopTimer();	
		/*//b = new ChessBoard();
		remove(clocks[0]);
		remove(clocks[1]);
		clocks[0] = new Clock(player1Name);
		clocks[1] = new Clock(player2Name);
		add(clocks[0], BorderLayout.NORTH);
		add(clocks[1], BorderLayout.SOUTH);
		repaint();
		sound(START);*/
	}
	
	/** this methpod will ask for user to enter their name*/
	public void getPlayer1Name()
	{
		player1Name = JOptionPane.showInputDialog(null, "Please enter Player 1 name");
	}
	
	/** this methpod will ask for user to enter their name*/
	public void getPlayer2Name()
	{
		player2Name = JOptionPane.showInputDialog(null, "Please enter Player 2 name");
	}
	
	//----- inner class for the mouse listener and mouse motion listener------
	class MouseClickListener implements MouseListener, MouseMotionListener
	{
		/** this method will select the piece if it is valid(has his turn) and will start the timer and will animate the piece to the final destination*/
		public void mouseClicked(MouseEvent event)
		{	
			try
			{	
				//System.out.println("canMove " + canMove + " onlineMove " + onlineMove);
				if(drag == false && canMove == true )//&& onlineMove == true) // if user is not dragging and piece is valid
				{	
					MCOORDS[0] = MCOORDS[2];
					MCOORDS[1] = MCOORDS[3];
					MCOORDS[2] = (int)((event.getX()-Y)/45)-1;
					MCOORDS[3] = (int)((event.getY()-X)/45)-1;
			
					// change the image of the background
					if(b.checkSamePiece(MCOORDS[1],MCOORDS[0],MCOORDS[3],MCOORDS[2]) == true)
					{	
						Piece p = b.getPiece(MCOORDS[3],MCOORDS[2]);
						
						if(p.getColor() == getTurn())
						{	
							b.selectPiece(MCOORDS[3],MCOORDS[2]);
							ai.onClicked(p,true,false);
							repaint();
						}	
						
						if(MCOORDS[0] > -1 && MCOORDS[1] > -1)// if we have final and initial coordinations	
						{	
							if(b.canMove(getTurn(),MCOORDS[1],MCOORDS[0],MCOORDS[3],MCOORDS[2]) == true)
							{
								try{
									startTimer(MCOORDS);	
									shareData(MCOORDS[1],MCOORDS[0],MCOORDS[3],MCOORDS[2]);
								}
								catch(Exception e)
								{}
							}
							
							MCOORDS[2] = -1;
							MCOORDS[3] = -1;
						}
					}	
					else
					{
						b.unSelectPiece();
						b.unSelectPossible();
						repaint();
						MCOORDS[2] = -1;
						MCOORDS[3] = -1;		
					}
				}	
			}	
			catch(ArrayIndexOutOfBoundsException e)
			{}
		}
			
		public void mousePressed(MouseEvent event) {
			
			initialY = (int)((event.getX()-Y)/45)-1;
			initialX = (int)((event.getY()-X)/45)-1;
			sound(CLICK);
		}
		
		// do noting
		public void mouseEntered(MouseEvent event){}
		
		public void mouseExited(MouseEvent event) {}
		
		/** this will drag the piece*/
		public void mouseDragged(MouseEvent event)
		{	
			MCOORDS[2] = -1;
			MCOORDS[3] = -1;
			
			if(drag != false)
			{
				b.unSelectPiece();// change the background color back to original where pointer is no more
				repaint();
			}
			
			draggedX = (int)((event.getX()-Y)/45)-1;
			draggedY = (int)((event.getY()-X)/45)-1;
			
			if(canMove == true && b.checkBoundary(draggedY,draggedX) == true && onlineMove == true) //if there is a piece and final destination is not outside the board
			{
				drag = true;	
				b.selectPiece(draggedY,draggedX);
				repaint();
			}
			else
				drag = false;	
		}	
		
		/** after getting the drag this method will simply move the piece to that loction*/
		public void mouseReleased(MouseEvent event) 
		{
			if(drag == true && canMove == true)
			{	
				if(b.canMove(getTurn(),initialY,initialX,draggedY,draggedX) == true)
				{
					try
					{
						shareData(initialY,initialX,draggedY,draggedX);
					}
					catch(Exception e)
					{
						System.out.println(e.getMessage());
					}

					clocks[getTurn()].stopTimer();

					b.move(initialY,initialX,draggedY,draggedX);
						

					if(b.locateKing(getTurn()) == null)
					{
						timeUp();
						clocks[getTurn()].setText("WIN");	
						moves++;
						clocks[getTurn()].setText("LOOSE");
						JOptionPane.showMessageDialog(null, "Game Over");
					}
					else
					{
						moves++;
						startGame(); // start the clock
					}	
				}
				drag = false;	
			}
		}
		
		public void mouseMoved(MouseEvent event)
		{
			try{
			getOnlineInput();
			ifNotInTime();
		}
		catch(Exception e)
		{}
		}   
	}

	public void ifNotInTime()
	{
		if(getInTimeW() == false || getInTimeB() == false) // if piece is not moved within  time
				timeUp();
	}

	/** 
	 ** this method will return a boolean value that will determine if piece is moved within time
	 ** @return clocks[0].getInTime(), the in time status of clock
	*/
	public boolean getInTimeW() 
	{
		return clocks[0].getInTime();
	}


	/** 
	 ** this method will return a boolean value that will determine if piece is moved within time
	 ** @return clocks[0].getInTime(), the in time status of clock
	*/
	public boolean getInTimeB()
	{
		return clocks[1].getInTime();
	}
	
	/** this method will draw the selectd piece after some delay looks moving the piece from starting position to the final position*/
	public void startTimer(int[] MCOORDS)
	{
		clocks[getTurn()].setPlayerName();

		clocks[getTurn()].stopTimer();// stop the timer of the player who has turn 
		final int[] positionXY = new int[4];
		final int[] difference = new int[2];// the differene between final and intial position
		
		positionXY[0] = b.getPiece(MCOORDS[1],MCOORDS[0]).getX();// gives the row selected
		positionXY[1] = b.getPiece(MCOORDS[1],MCOORDS[0]).getY(); // gives the column selected

		positionXY[2] = ((MCOORDS[2]+1)*45)+Y; // actual x position on frame
		positionXY[3] = ((MCOORDS[3]+1)*45)+X; // actual y position on frame

		difference[0] = positionXY[2] - positionXY[0];// x difference
		difference[1] = positionXY[3] - positionXY[1];// y diference

		class TimerListener implements ActionListener
		{
			int counter = 0;//how many time piece is animated
			final int tempX = MCOORDS[0];
			final int tempY = MCOORDS[1];

			public void actionPerformed(ActionEvent event)
			{
				counter++;
				if(counter <= Math.abs(difference[0]))// if there is still difference left between final and initial x 
				{
					positionXY[0] = positionXY[0] + difference[0]/Math.abs(difference[0]);
				}
				
				if(counter <= Math.abs(difference[1]))// if there is still difference left between final and initial y
				{		
					positionXY[1] = positionXY[1] + difference[1]/Math.abs(difference[1]);
				}

				b.animate(tempY,tempX,positionXY[0],positionXY[1]);// animating the piece
				repaint();
						
				if(Math.abs(positionXY[2] - positionXY[0]) < 5 && Math.abs(positionXY[3] - positionXY[1]) < 5)// checking the difference between intial and final positions with tolerance 15
				{
					try
					{
						b.move(tempY,tempX,(int)((positionXY[3]-X)/45)-1,(int)((positionXY[2]-Y)/45)-1);
								
						sound(MOVE);
						repaint();
						t.stop();
					}
					finally
					{
						onlineMove = false;
						MCOORDS[2] = -1;
						MCOORDS[3] = -1;

						if(b.locateKing(getTurn()) == null)
						{
							timeUp();
							clocks[getTurn()].setText("WIN");	
							moves++;
							clocks[getTurn()].setText("LOOSE");
							JOptionPane.showMessageDialog(null, "Game Over");
						}
						else
						{
							moves++;
							startGame(); // start the clock
						}	
					}	
				}	
			}
		}
		t = new Timer(0,new TimerListener());	
		t.start();
	}
	
	/** this method will simply call the method loadGame() of board*/
	public void loadGame()  throws IOException
	{
		b.loadGame();
	}

	/** this method will simply call the saveGame() method in Board*/
	public void saveGame() throws IOException
	{
		b.saveGame();
	}
	
	public void changeMcoords(Piece p)
	{
		MCOORDS[0] = (int)((p.getX()-Y)/45)-1;
		MCOORDS[1] = (int)((p.getY()-X)/45)-1;
		MCOORDS[3] = p.getAvailableOptions()[0];
		MCOORDS[2] = p.getAvailableOptions()[1];
	}

	public void resetMcoords()
	{
		MCOORDS[0]=-1;MCOORDS[1]=-1;MCOORDS[2]=-1;MCOORDS[3] = -1;
	}

	/** this method returns who has the turn
	 ** @return turn[0], the turn
	 */
	public int getTurn()
	{	
		// change the turn after every valid move
		if(moves % 2 == 0)
		{
			turn[0] = 0;
		}
		else
		{
			turn[0] = 1;
		}

		return turn[0];
	}

	public int getworkAs()
	{
		return workAs;
	}

	/** client */
	public void clientSetup(String serverIP, int port_number)
	{
		try
		{
			s = new Socket(serverIP,port_number);
			in = new Scanner(s.getInputStream());
	    	out = new PrintWriter(s.getOutputStream());
	    	reader = new BufferedReader(new InputStreamReader(s.getInputStream()));
			workAs = Integer.parseInt(in.nextLine());
						
			if(workAs == 0)
			{
				out.println(player2Name);
				out.flush();
				player1Name = in.nextLine();
			}	
			else
			{
				out.println(player1Name);
				out.flush();
				player2Name = in.nextLine();
			}	
	   		b.setWorkAs(workAs);
	   		//reStart();
		}	
	    catch(Exception e)
	    {}	
	}
	
	public void shareData(int y1, int x1, int y2, int x2)
	{	
		String sending;
		try
		{
			if(getTurn() != workAs)
			{	
				onlineMove = false;
				sending = y1 + " " + x1 + " " + y2 + " " + x2;
				out.println(sending);
				out.flush();
			}		
		}	
		catch(Exception e)
		{
			System.out.println("\n Response: Quit");
		} 
	}
	
	public void getOnlineInput() throws Exception
	{	
		if(reader.ready())
		{	
			MCOORDS[1] = Integer.parseInt(in.next());
			MCOORDS[0] = Integer.parseInt(in.next());
			MCOORDS[3] = Integer.parseInt(in.next());
			MCOORDS[2] = Integer.parseInt(in.next()); 
			
			startTimer(MCOORDS);
			try{
			check.interrupt();
		}
		catch(Exception e)
		{}
		}
	}

	public void goOnline()
	{
		clientSetup("localhost",9997);
		online = true;
		onlineMove = false;
	}
	
	/** 
	 ** this method overwrites the  paintComponent method 
	 ** and calls every piece to draw itself
	 */
	public void paintComponent(Graphics h) {
		Graphics2D g = (Graphics2D)h;
		b.paint(g);
	}

	public void startCheckingInput()
	{
		class CheckInput implements Runnable
		{
			public void run()
			{
				try
				{	
					while(true)
					{	
						if(getTurn() == getworkAs())
						{
							if(getInTimeW() == false || getInTimeB() == false) // if piece is not moved within  time
								timeUp();
								
							onlineMove = false;
							getOnlineInput();
						}
						Thread.sleep(100);	
					}	
				}
				catch(Exception e)
				{}
			}
		}
		CheckInput c = new CheckInput();
		check = new Thread(c);
		check.start();
	}

	public static void sound(String file){
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(file).getAbsoluteFile( ));
			Clip clip = AudioSystem.getClip( );
			clip.open(audioInputStream);
			clip.start( );
		}
		catch(Exception e){
			e.printStackTrace( );
		}
	}

	public void moveAfterDelay()
	{	
		class Delay implements Runnable
		{
			public void run()
			{
				try
				{	
					
					Thread.sleep((int)(1000 + Math.random() * 5000));	
					startTimer(MCOORDS);
					delayThread.stop();	
				}
				catch(Exception e)
				{}
			}
		}
		Delay d = new Delay();
		delayThread = new Thread(d);
		delayThread.start();
	}	
}