/**
 ** @name Pawn
 ** @author Devinder Singh
 ** this class defines chess Piece Bishop's properties
 */
import java.awt.Graphics2D; 

public class Pawn extends Piece
{
	// declaring variables
	private static int id = 0;
	private int type = 6;
	private int color;
	private BackGround p; 
	private int[] availableOption = {-1,-1};
	
	// constructor will call the super class constructor and will define its name
	public Pawn(int color, int x, int y)
	{
		this.color = color;
		
		id++;

		if(color == 0)
			p = new BackGround("image/wP.png",x,y);
		else
			p = new BackGround("image/bP.png",x,y);
	}
	
	public int getID()
	{
		return id;
	}
	
	/** this return the type of the piece
	 ** @return type
	 */
	public int getType()
	{
		return type;
	}

	/** this metod will set x and y of the image*/
	public void setXY(int x, int y)
	{
		p.setXY(x,y);
	}
	
	/** this method return the color of the piece
	 ** @return color
	*/
	public int getColor()
	{
		return color;
	}
	
		public int moveUpTo()
	{
		return 1;
	}

	public int[] pattern(int pattern,int x, int y)
	{
		int [] xy = {x,y};

		switch(pattern)
		{
			case 1:
				xy[0]++;
				xy[1]++;
				break;
			case 2:
				xy[0]++;
				xy[1]--;
				break;
			case 3:
				xy[0]--;
				xy[1]++;
				break;
			case 4:
				xy[0]--;
				xy[1]--;
				break;
			case 6:
				xy[0]++;
				break;
			case 7:
				xy[0]--;
				break;
			case 8:
				xy[0]+=2;
				break;
			case 9:
				xy[0]-=2;
				break;				
		}
		return xy;					
	}

	public int numOfPat()
	{
		return 9; 
	}

	/** this method @return p.getX(), the x coords of the piece*/
	public int getX()
	{
		return p.getX();
	}
	
	/** this method @return p.getX(), the y coords of the piece*/
	public int getY()
	{
		return p.getY();
	}
	
	/** this method @return p.getX(), the x coords of the piece*/
	public int getArrayX()
	{
		return (int)((p.getX()-400)/45)-1;
	}
	
	/** this method @return p.getX(), the y coords of the piece*/
	public int getArrayY()
	{
		return (int)((p.getY()-150)/45)-1;
	}
	
	public void setAvailableOptions(int[] xy)
	{
		availableOption = xy;
	}

	public int[] getAvailableOptions()
	{
		return availableOption;
	}
	
	/**this method will decide can the piece can move or not*/
	public boolean canMove(int dx, int dy,Piece[][] piece)
	{
		if(color == 0 && dx - getArrayY() < 3 && dx - getArrayY() > 0  && dy - getArrayX() == 0 && piece[dx][dy].getType() == -1)
		{
			if(getArrayY() > 2 && dx - getArrayY() > 1)
				return false;	
			return true;
		}
			
		if(color == 1 && dx - getArrayY() < 0 && dx - getArrayY() > -3 && dy - getArrayX() == 0 && piece[dx][dy].getType() == -1)
		{
			if(getArrayY() < 3 && dx - getArrayY() < -1)
				return false;	
			return true;
		}	

		if(dx - getArrayY() == 1 && color == 0 && Math.abs(dy - getArrayX()) == 1)
			if(piece[dx][dy].getType() != -1 && piece[dx][dy].getColor() != color)
				return true;

		if(dx - getArrayY() == -1 && color == 1 && Math.abs(dy - getArrayX()) == 1)
			if(piece[dx][dy].getType() != -1 && piece[dx][dy].getColor() != color)
				return true;

		return false;
	}
	
	/** this method calls the paint method of the backGround class 
	** @param Graphics2D
	*/
	public void paint(Graphics2D g)
	{
		p.paint(g);
	}
}	